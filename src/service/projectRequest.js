import $http from '../utils/http.js'
const projectUrl='/jira/api/project'
const projectRequest = {
    getBigTypeList(params){
        return $http.get(`${projectUrl}/searchProjectBigtype`, {params})
    },
    getProjectsList(params){
        return $http.get(`${projectUrl}/searchProject`, {params})
    },
    updateProjectBigType(params){
        return $http.get(`${projectUrl}/updateProject`, {params})
    },
    deleteProjectBigtype(params){
        return $http.get(`${projectUrl}/deleteProjectBigtype`, {params})
    },
    updateProjectBigtype(params){
        return $http.get(`${projectUrl}/updateProjectBigtype`, {params})
    },
    addProjectBigtype(params){
        return $http.get(`${projectUrl}/addProjectBigtype`, {params})
    },

}
export default projectRequest
