import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    bigType:[]
  },
  mutations: {
    updateBigTypeList(state,data){
      state.bigType=data
    }
  },
  actions: {
  },
  modules: {
  }
})
