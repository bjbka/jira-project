const path = require("path")

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    publicPath: "./",
    assetsDir: "static",
    outputDir: 'dist',
    devServer: {
        proxy: {
            '/api': {
                target: 'http://81.70.11.128:5000',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                },
            }
        }
    },
    chainWebpack: (config) => {
        config.resolve.alias
            .set("@", resolve("./src"))
    },
    productionSourceMap: false,
    // presets: ["@vue/app"],
    // plugins: [
    //     ["import",
    //         {libraryName: "ant-design-vue", libraryDirectory: "es", style: true}
    //     ]
    // ]
}